const fs = require('fs')
const path = require('path')

const allUserDataPath = path.join(__dirname, './alluserdata')

/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/

// function getEachUserData(each){
//     return new Promise((res,rej)=>{
//         fetch()
//     })
// }

// function getDetailsForEachUser(usersData){
//     return new Promise((resolve,reject)=>{
//         let detailsPromises=usersData.map(eachUser=>{
//             getEachUserData(eachUser)
//         })
//         Promise.all(detailsPromises)
//         .then(()=>{
//             resolve()
//         })
//         .catch(err){
//             reject(err)

//         }
//     })
// }


function createFie(filename, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(path.join(__dirname, filename), JSON.stringify(data), (err) => {
            if (err) {
                console.err(err)
                reject(err)
            } else {
                resolve()
            }
        })

    })
}

//1. Fetch all the users

fetch('https://jsonplaceholder.typicode.com/users')
    .then((response) => {
        return response.json()
    })
    .then((usersData) => {
        return createFie('./alluserdata.json', usersData)
    })
    .catch((err) => {
        console.log(err)
    })

// 2. Fetch all the todos  

let todosRespose = fetch('https://jsonplaceholder.typicode.com/todos')
    .then((todosResponse) => {
        return todosResponse.json()
    })
    .then((todosData) => {
        return createFie('./todosdata.json', todosData)
    })
    .catch((err) => {
        console.log(err)
    })

//3. Use the promise chain and fetch the users first and then the todos.

fetch('https://jsonplaceholder.typicode.com/users')
    .then((response) => {
        return response.json()
    })
    .then((usersData) => {
        return createFie('./alluserdata1.json', usersData)
    })
    .catch((err) => {
        console.log(err)
    })
    .then(() => {
        return fetch('https://jsonplaceholder.typicode.com/todos')
    })
    .then((todosResponse) => {
        return todosResponse.json()
    })
    .then((todosData) => {
        return createFie('./todosdata1.json', todosData)
    })
    .catch((err) => {
        console.log(err)
    })

//4. Use the promise chain and fetch the users first and then all the details for each user.

// fetch('https://jsonplaceholder.typicode.com/users')
//     .then((response) => {
//         return response.json()
//     })
//     .then((usersData) => {
//         getDetailsForEachUser(usersData)
//     })
    
//     .catch((err) => {
//         console.log(err)
//     })






